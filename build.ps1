# SPDX-License-Identifier: MIT
# Copyright (c) 2022 Mikhail Paulyshka



Import-Module "$PSScriptRoot/src_build/library.psm1" -Force -DisableNameChecking
Build-Package -PackageDirectory "$PSScriptRoot/src" -OutputDirectory "$PSScriptRoot/~output"
