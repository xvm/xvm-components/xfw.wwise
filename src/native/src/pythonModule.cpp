// SPDX-License-Identifier: MIT
// Copyright (c) 2020-2024 XVM Team

#include <pybind11/pybind11.h>

#include "wwise.h"

PYBIND11_MODULE(XFW_WWISE, m) {
	m.doc() = "XFW WWise module";

	pybind11::class_<Wwise>(m, "WWiseNative")
		.def(pybind11::init<>())
		
		.def("bank_load", &Wwise::BankLoad, "Load WWise bank by filepath relative to WorldOfTanks.exe")
		.def("bank_unload", &Wwise::BankUnload, "Unload WWise bank by bankID.")
		.def("bank_unload_by_name", &Wwise::BankUnloadByName, "Unload WWise bank by name.")

		.def("comm_init", &Wwise::CommunicationInit, "Start communication wtih Audiokinetic WWISE Authoring Tools");
}
