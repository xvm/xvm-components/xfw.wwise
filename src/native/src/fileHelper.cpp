// SPDX-License-Identifier: MIT
// Copyright (c) 2020-2022 XVM Team

#include <Windows.h>

#include "fileHelper.h"

namespace FileHelper {

    bool AreEqual(const std::filesystem::path& first, const std::filesystem::path& second)
    {
        WIN32_FILE_ATTRIBUTE_DATA info_first{};
        WIN32_FILE_ATTRIBUTE_DATA info_second{};

        if (!GetFileAttributesExW(first.wstring().c_str(), GetFileExInfoStandard, (void*)&info_first) ||
            !GetFileAttributesExW(second.wstring().c_str(), GetFileExInfoStandard, (void*)&info_second))
        {
            return false;
        }

        //size
        if ((info_first.nFileSizeHigh != info_second.nFileSizeHigh) ||
            (info_first.nFileSizeLow != info_second.nFileSizeLow))
        {
            return false;
        }

        //last writing time
        if (CompareFileTime(&(info_first.ftLastWriteTime), &(info_second.ftLastWriteTime)) != 0)
        {
            return false;
        }

        return true;
    }

    bool CreateHardlink(const std::filesystem::path& path_from, const std::filesystem::path& path_to)
    {
        if (GetDriveTypeW(path_from.root_path().wstring().c_str()) >= DRIVE_REMOTE) {
            return false;
        }
        if (GetDriveTypeW(path_to.root_path().wstring().c_str()) >= DRIVE_REMOTE) {
            return false;
        }

        if (!OnNTFS(path_from.root_path())) {
            return false;
        }
        if (!OnNTFS(path_to.root_path())) {
            return false;
        }

        if (path_from.root_path() != path_to.root_path()) {
            return false;
        }

        return (CreateHardLinkW(path_to.wstring().c_str(), path_from.wstring().c_str(), NULL) != FALSE);
    }

    bool OnNTFS(const std::filesystem::path& path)
    {
        wchar_t fs[MAX_PATH]{};

        if (!GetVolumeInformationW(path.root_path().wstring().c_str(), NULL, NULL, NULL, NULL, NULL, fs, _countof(fs))) {
            return false;
        }

        if (_wcsnicmp(fs, L"NTFS", 4) != 0) {
            return false;
        }

        return true;
    }

    std::wstring RemoveExtension(const std::wstring& filename) {
        size_t lastdot = filename.find_last_of(L".");
        if (lastdot == std::wstring::npos) {
            return filename;
        }
        return filename.substr(0, lastdot);
    }
}