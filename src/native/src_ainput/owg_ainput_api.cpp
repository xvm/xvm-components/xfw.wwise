// Copyright (c) 2025 Audiokinetic Inc.
// Copyright (c) 2025 OpenWG Team

//
// Includes
//

// OpenWG.WoT.Audio
#include "owg_ainput_api.h"



//
// Implementation
//


/*


// Holds audio input information
struct AudioInputStream
{
AkReal32 *		m_pData;
AkUInt32		m_uDataSize;
AkUInt32		m_uSampleRate;
AudioInputStream() : m_pData(NULL), m_uDataSize(0), m_uSampleRate(0) {}
};

AkAudioInputPluginExecuteCallbackFunc CAkFXSrcAudioInput::m_pfnExecCallback = NULL;
AkAudioInputPluginGetFormatCallbackFunc CAkFXSrcAudioInput::m_pfnGetFormatCallback = NULL;
AkAudioInputPluginGetGainCallbackFunc CAkFXSrcAudioInput::m_pfnGetGainCallback = NULL;

if( m_pfnGetFormatCallback )
        {
            m_pfnGetFormatCallback( in_pSourceFXContext->GetVoiceInfo()->GetPlayingID(), io_rFormat );
        }

void CAkFXSrcAudioInput::SetAudioInputCallbacks(
                    AkAudioInputPluginExecuteCallbackFunc in_pfnExecCallback,
                    AkAudioInputPluginGetFormatCallbackFunc in_pfnGetFormatCallback,
                    AkAudioInputPluginGetGainCallbackFunc in_pfnGetGainCallback
                    )
 {
     if( in_pfnExecCallback && in_pfnGetFormatCallback )
     {
         m_pfnExecCallback =        in_pfnExecCallback;
         m_pfnGetFormatCallback =   in_pfnGetFormatCallback;
         m_pfnGetGainCallback =     in_pfnGetGainCallback;
     }
 }

void SetAudioInputCallbacks(
                AkAudioInputPluginExecuteCallbackFunc in_pfnExecCallback,
                AkAudioInputPluginGetFormatCallbackFunc in_pfnGetFormatCallback , // Optional
                AkAudioInputPluginGetGainCallbackFunc in_pfnGetGainCallback   // Optional
                )
{
    CAkFXSrcAudioInput::SetAudioInputCallbacks( in_pfnExecCallback, in_pfnGetFormatCallback, in_pfnGetGainCallback );
}

*/