// Copyright (c) 2025 Audiokinetic Inc.
// Copyright (c) 2025 OpenWG Team

#pragma once

//
// Includes
//

// Audiokinetic
#include <AK/SoundEngine/Common/IAkPlugin.h>

// OpenWG.WoT.Audio
#include "owg_ainput_params.h"



//
// Class
//

namespace OpenWG::Audio::AInput {
	class AInputPlugin : public AK::IAkSourcePlugin
	{
	public:
		// Constructor.
		AInputPlugin();
		~AInputPlugin() override;

		// AK::IAkPlugin
		AKRESULT Term( AK::IAkPluginMemAlloc * in_pAllocator ) override;
		AKRESULT Reset( ) override;
		AKRESULT GetPluginInfo( AkPluginInfo & out_rPluginInfo ) override;
		bool SupportMediaRelocation() const override;
		AKRESULT RelocateMedia(AkUInt8* in_pNewMedia, AkUInt8* in_pOldMedia) override;

		// AK::IAkSourcePlugin
		AKRESULT Init(	AK::IAkPluginMemAlloc *			in_pAllocator,
						AK::IAkSourcePluginContext *	in_pSourceFXContext,
						AK::IAkPluginParam *			in_pParams,
						AkAudioFormat &					io_rFormat
						) override;

		AkReal32 GetDuration() const override;
		AkReal32 GetEnvelope() const override;
		AKRESULT StopLooping() override;
		AKRESULT Seek(AkUInt32 in_uPosition) override;
		AKRESULT TimeSkip(AkUInt32 &) override;
		void Execute( AkAudioBuffer *	io_pBuffer ) override;;

	private:
		AK::IAkSourcePluginContext * m_pSourceFXContext{};

		AInputParams * m_pSharedParams{};

		AkAudioFormat m_Format{};
	};
}
