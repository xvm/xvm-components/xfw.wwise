// Copyright (c) 2025 Audiokinetic Inc.
// Copyright (c) 2025 OpenWG Team

//
// Includes
//

// Audiokinetic
#include <AK/SoundEngine/Common/IAkPlugin.h>

// OpenWG.WoT.Audio
#include "owg_ainput_authoring.h"
#include "owg_ainput_constants.h"
#include "owg_ainput_params.h"
#include "owg_ainput_plugin.h"


//
// Global
//

// Plugin mechanism. FX create function and register its address to the FX manager.
AK::IAkPlugin* owg_ainput_create_plugin( AK::IAkPluginMemAlloc * in_pAllocator )
{
	return AK_PLUGIN_NEW( in_pAllocator, OpenWG::Audio::AInput::AInputPlugin() );
}

// Plugin mechanism. Parameter node create function to be registered to the FX manager.
AK::IAkPluginParam * owg_ainput_create_params(AK::IAkPluginMemAlloc * in_pAllocator)
{
	return AK_PLUGIN_NEW(in_pAllocator, OpenWG::Audio::AInput::AInputParams());
}

AK_ATTR_USED AK::PluginRegistration owg_ainputRegistration(
	AkPluginTypeSource,
	AKCOMPANYID_OPENWG,
	AKPLUGINID_OPENWG_AINPUT,
	&owg_ainput_create_plugin,
	&owg_ainput_create_params
);


AK_DEFINE_PLUGIN_CONTAINER(owg_ainput);
AK_EXPORT_PLUGIN_CONTAINER(owg_ainput);

AK_ADD_PLUGIN_CLASS_TO_CONTAINER(owg_ainput, OpenWG::Audio::AInput::AInputAuthoringPlugin, owg_ainput);

DEFINE_PLUGIN_REGISTER_HOOK;

DEFINEDUMMYASSERTHOOK;
