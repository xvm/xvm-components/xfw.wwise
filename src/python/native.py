"""
SPDX-License-Identifier: MIT
Copyright (c) 2020-2024 XVM Team
"""

#
# Includes
#

# stdlib
import importlib

# XFW.Native
from xfw_native.python import XFWNativeModuleWrapper



#
# Globals
#

g_xfw_wwise_native_module = None
g_xfw_wwise_native_obj = None
g_xfw_wwise_native_inited = False



#
# Initialization
#

def init():
    global g_xfw_wwise_native_module
    global g_xfw_wwise_native_obj
    global g_xfw_wwise_native_inited

    g_xfw_wwise_native_module = XFWNativeModuleWrapper('com.modxvm.xfw.wwise', 'xfw_wwise.pyd', 'XFW_WWISE')
    if g_xfw_wwise_native_module is not None:
        g_xfw_wwise_native_obj = g_xfw_wwise_native_module.WWiseNative()
        if g_xfw_wwise_native_obj is not None:
            g_xfw_wwise_native_inited = True


def fini():
    global g_xfw_wwise_native_module
    global g_xfw_wwise_native_obj
    global g_xfw_wwise_native_inited
    g_xfw_wwise_native_obj = None
    g_xfw_wwise_native_module = None
    g_xfw_wwise_native_inited = False


def is_inited():
    global g_xfw_wwise_native_inited
    return g_xfw_wwise_native_inited



#
# Public
#

def wwise_communication_init():
    if not is_inited():
        return False

    return g_xfw_wwise_native_obj.comm_init()


def wwise_native():
    if not is_inited():
        return None

    return g_xfw_wwise_native_obj
