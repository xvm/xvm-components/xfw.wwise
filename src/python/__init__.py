"""
SPDX-License-Identifier: MIT
Copyright (c) 2020-2024 XVM Team
"""

#
# Imports
#

# XFW.WWise
import native
import bank_manager



#
# API
#

def wwise_bank_add(bank_path, add_to_battle, add_to_hangar): # -> Bool
    return bank_manager.wwise_bank_add(bank_path, add_to_battle, add_to_hangar)


def wwise_bank_remove(bank_path): # -> Bool
    return bank_manager.wwise_bank_remove(bank_path)


def wwise_bank_unload(bank_name): # -> Bool
    return bank_manager.wwise_bank_unload(bank_name)


def wwise_communication_init(): # -> Bool
    return native.wwise_communication_init()


def wwise_native(): # -> Object
    return native.wwise_native()



#
# XFW Loader
#

def xfw_is_module_loaded():
    import native
    if not native.is_inited():
        return False

    import bank_manager
    if not bank_manager.is_inited():
        return False

    return True


def xfw_module_init():
    import native
    native.init()

    import bank_manager
    bank_manager.init()


def xfw_module_fini():
    import bank_manager
    bank_manager.fini()

    import native
    native.fini()
